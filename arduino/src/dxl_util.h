#ifndef DYNAMIXEL_ESP_SRC_DXL_UTIL_H_
#define DYNAMIXEL_ESP_SRC_DXL_UTIL_H_

// Please modify it to suit your hardware.
#if defined(ARDUINO_AVR_UNO) ||                                                \
    defined(ARDUINO_AVR_MEGA2560) // When using DynamixelShield
#include <SoftwareSerial.h>
SoftwareSerial soft_serial(7, 8); // DYNAMIXELShield UART RX/TX
#define DXL_SERIAL Serial
#define DEBUG_SERIAL soft_serial
const int DXL_DIR_PIN = 2;     // DYNAMIXEL Shield DIR PIN
#elif defined(ARDUINO_SAM_DUE) // When using DynamixelShield
#define DXL_SERIAL Serial
#define DEBUG_SERIAL SerialUSB
const int DXL_DIR_PIN = 2; // DYNAMIXEL Shield DIR PIN
#elif defined(ARDUINO_SAM_ZERO) // When using DynamixelShield
#define DXL_SERIAL Serial1
#define DEBUG_SERIAL SerialUSB
const int DXL_DIR_PIN = 2; // DYNAMIXEL Shield DIR PIN
#elif defined(                                                                 \
    ARDUINO_OpenCM904) // When using official ROBOTIS board with DXL circuit.
#define DXL_SERIAL                                                             \
  Serial3 // OpenCM9.04 EXP Board's DXL port Serial. (Serial1 for the DXL port
          // on the OpenCM 9.04 board)
#define DEBUG_SERIAL Serial
const int DXL_DIR_PIN = 22; // OpenCM9.04 EXP Board's DIR PIN. (28 for the DXL
                            // port on the OpenCM 9.04 board)
#elif defined(                                                                 \
    ARDUINO_OpenCR) // When using official ROBOTIS board with DXL circuit.
// For OpenCR, there is a DXL Power Enable pin, so you must initialize and
// control it. Reference link :
// https://github.com/ROBOTIS-GIT/OpenCR/blob/master/arduino/opencr_arduino/opencr/libraries/DynamixelSDK/src/dynamixel_sdk/port_handler_arduino.cpp#L78
#define DXL_SERIAL Serial3
#define DEBUG_SERIAL Serial
const int DXL_DIR_PIN = 84; // OpenCR Board's DIR PIN.
#elif defined(ARDUINO_OpenRB) // When using OpenRB-150
// OpenRB does not require the DIR control pin.
#define DXL_SERIAL Serial1
#define DEBUG_SERIAL Serial
const int DXL_DIR_PIN = -1;
#else // Other boards when using DynamixelShield
#define DXL_SERIAL Serial1
#define DEBUG_SERIAL Serial
const int DXL_DIR_PIN = 2; // DYNAMIXEL Shield DIR PIN
#endif

namespace dxl_util {

const float DXL_PROTOCOL_VERSION = 2.0;
Dynamixel2Arduino dxl(DXL_SERIAL, DXL_DIR_PIN);

void init() {
  // Set Port baudrate to 57600bps. This has to match with DYNAMIXEL baudrate.
  dxl.begin(57600);
  // Set Port Protocol Version. This has to match with DYNAMIXEL protocol
  // version.
  dxl.setPortProtocolVersion(DXL_PROTOCOL_VERSION);
}

bool positionMode(uint8_t id) {
  // Turn off torque when configuring items in EEPROM area
  dxl.torqueOff(id);
  dxl.setOperatingMode(id, OP_POSITION);
  dxl.torqueOn(id);
  return setProfileVelocityAcceleration(id, 90, 0);
}

bool extendedPositionMode(uint8_t id) {
  // Turn off torque when configuring items in EEPROM area
  dxl.torqueOff(id);
  dxl.setOperatingMode(id, OP_EXTENDED_POSITION);
  dxl.torqueOn(id);
  return setProfileVelocityAcceleration(id, 90, 0);
}

bool setGoalPosition(uint8_t id, int32_t pos) {
  // Please refer to
  // e-Manual(http://emanual.robotis.com/docs/en/parts/interface/dynamixel_shield/)
  // for available range of value. Set Goal Position in RAW value
  return dxl.setGoalPosition(id, pos);
}

int32_t getPresentPosition(uint8_t id) {
  return static_cast<int32_t>(dxl.getPresentPosition(id));
}

bool ping(uint8_t id) { return dxl.ping(id); }

bool setID(uint8_t id, uint8_t new_id) { return dxl.setID(id, new_id); }

bool setProfileVelocityAcceleration(uint8_t id, uint32_t velocity,
                                    uint32_t acceleration) {
  return dxl.writeControlTableItem(ControlTableItem::PROFILE_VELOCITY, id,
                                   velocity) &&
         dxl.writeControlTableItem(ControlTableItem::PROFILE_ACCELERATION, id,
                                   acceleration);
}

int32_t moving(uint8_t id) {
  return dxl.readControlTableItem(ControlTableItem::MOVING, id);
}

uint32_t movingStatus(uint8_t id) {
  return dxl.readControlTableItem(ControlTableItem::MOVING_STATUS, id);
}

uint32_t getHomingOffset(uint8_t id) {
  return dxl.readControlTableItem(ControlTableItem::HOMING_OFFSET, id);
}

bool setHomingOffset(uint8_t id, uint32_t offset) {
  return dxl.writeControlTableItem(ControlTableItem::HOMING_OFFSET, id, offset);
}

int32_t presentLoad(uint8_t id) {
  return dxl.readControlTableItem(ControlTableItem::PRESENT_LOAD, id);
}

int32_t presentTemperature(uint8_t id) {
  return dxl.readControlTableItem(ControlTableItem::PRESENT_TEMPERATURE, id);
}

int32_t presentVoltage(uint8_t id) {
  return dxl.readControlTableItem(ControlTableItem::PRESENT_VOLTAGE, id);
}

int32_t hardwareErrorStatus(uint8_t id) {
  return dxl.readControlTableItem(ControlTableItem::HARDWARE_ERROR_STATUS, id);
}

ModelInfo getModelInfo(uint8_t id) {
  ModelInfo model_info;
  model_info.model_number = dxl.getModelNumber(id);
  model_info.model_info =
      dxl.readControlTableItem(ControlTableItem::MODEL_INFORMATION, id);
  model_info.firmware_version =
      dxl.readControlTableItem(ControlTableItem::FIRMWARE_VERSION, id);
  model_info.protocol_type =
      dxl.readControlTableItem(ControlTableItem::PROTOCOL_VERSION, id);
  return model_info;
}

int32_t read(uint8_t id, uint8_t addr, uint8_t addr_length, uint8_t *resp,
             uint8_t resp_len) {
  return dxl.read(id, addr, addr_length, resp, resp_len);
}

bool write(uint8_t id, uint8_t addr, const uint8_t *req, uint8_t req_len) {
  return dxl.write(id, addr, req, req_len);
}

} // namespace dxl_util

#endif // DYNAMIXEL_ESP_SRC_DXL_UTIL_H_
