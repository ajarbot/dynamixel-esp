# dynamixel-esp

## About
Control dynamixel actuators remotely via WiFi. Uses these components:
* ESP32 (or ESP8266)
* OpenRB-150 https://emanual.robotis.com/docs/en/parts/controller/openrb-150/
* Dynamixel actuator

## How to Use
* Load the Arduino/main.ino to the OpenRB-150 board
* Load the micropython/main.py to the ESP board
* Connect the two boards via i2c:
  * OpenRB-150 i2c [pins](https://emanual.robotis.com/assets/images/parts/controller/openrb-150/openrb-150_pinout.png) pin 11 for SDA, pin12 for SCL
  * ESP can use any pins for i2c, since its all [software](https://docs.micropython.org/en/latest/esp8266/quickref.html#i2c-bus). By default pin 4 for SDA, pin 5 for SCL.
* Use the dxl-remote.py library to control the dynamixel from WiFi.
