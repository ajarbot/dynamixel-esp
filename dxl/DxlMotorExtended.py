from dxl.dxl_command import DxlCommand
import dxl.DxlMotor
import pickle


def _load_pkl_or_default(fname, default):
    try:
        with open(fname, 'rb') as f:
            return pickle.load(f)
    except IOError:
        return default


class PickledInt:
    def __init__(self, tag, default):
        self._fname = ".{}.dxl.pkl".format(tag)
        self._value = _load_pkl_or_default(self._fname, default)
        self._old = self._value

    def set(self, v):
        self._value = v
        if abs(self._value - self._old) > 200:
            self._save()

    def get(self):
        return self._value

    def _save(self):
        with open(self._fname, 'wb') as f:
            pickle.dump(self._value, f)
            self._old = self._value


class DxlMotorExtendedRaw(dxl.DxlMotor.DxlMotor):
    def __init__(self, remote, dxl_id) -> None:
        self._remote = remote
        self._dxl_id = dxl_id
        remote._send(
            DxlCommand.INIT_EXTENDED_POSITION_MODE, dxl_id)

    def computePresentPosition(self, raw_pos):
        return raw_pos

    def presentPosition(self):
        return self.computePresentPosition(self._present_position_raw())

    def setGoalPosition(self, pos):
        self._set_goal_position(pos)


class DxlMotorExtended(dxl.DxlMotor.DxlMotor):
    def __init__(self, remote, dxl_id) -> None:
        self._remote = remote
        self._dxl_id = dxl_id
        self._offset = 0
        self._pkl_pos = PickledInt(dxl_id, 0)
        if remote._send(
                DxlCommand.INIT_EXTENDED_POSITION_MODE, dxl_id) == None:
            print('1')
            return
        pos = self._present_position_raw()
        if pos == None:
            print('2')
            return
        best = 4096*4096
        best_pos = None
        print('h', pos, best, self._pkl_pos.get())
        for i in range(-128, 128):
            p = i*4096 + (pos % 4096)
            diff = abs(p - self._pkl_pos.get())
            if abs(best) >= diff:
                best = diff
                best_pos = p
        self._offset = pos - best_pos
        self._pkl_pos.set(best_pos)
        print('h', pos, best, best_pos, self._offset, self._pkl_pos.get())

    def computePresentPosition(self, raw_pos):
        raw_pos = self._present_position_raw()
        if raw_pos == None:
            return None
        raw_pos = raw_pos + self._offset
        self._pkl_pos.set(raw_pos)
        return raw_pos

    def presentPosition(self):
        return self.computePresentPosition(self._present_position_raw())

    def setGoalPosition(self, pos):
        self._set_goal_position(pos - self._offset)
