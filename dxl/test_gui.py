import tkinter as tk
from tkinter import ttk
import dxl
import functools
import time


class Global:
    def __init__(self) -> None:
        self.dxl_id_choice = None
        self.dxl_pos = None
        self.dxl_set_goal = None
        self.dxl = dxl.dxlremote.DxlRemote("10.0.0.212")
        self.dxl_motor = None
        self.dxl_homing_offset = None
        self.ids_found = []


gbl = Global()


def handle_rename(ent_from, ent_to, _):
    fr = int(ent_from.get())
    to = int(ent_to.get())
    print('rename', fr, to)
    res = gbl.dxl.set_id(fr, to)
    print(res)


def handle_ping(result_label, _):
    result_label.config(text="Pinging...")
    result_label.update()
    gbl.ids_found = []
    print('ping')
    for idd in range(15):
        r = gbl.dxl.ping(idd)
        print(idd, r)
        if r == 1:
            gbl.ids_found.append(idd)
    if not gbl.ids_found:
        return
    old = gbl.dxl_id_choice.get()
    gbl.dxl_id_choice['values'] = gbl.ids_found
    if int(old) in gbl.ids_found:
        gbl.dxl_id_choice.set(old)
    else:
        gbl.dxl_id_choice.set(gbl.ids_found[0])
    result_label.config(text=gbl.ids_found)
    result_label.update()
    on_motor_id_selected(None)


def on_motor_id_selected(_):
    idd = int(gbl.dxl_id_choice.get())
    if idd not in gbl.ids_found:
        return
    print('m', idd)
    if gbl.is_extended_mode.get() == 0:
        gbl.dxl_motor = gbl.dxl.motor_mode_position(idd)
    else:
        gbl.dxl_motor = gbl.dxl.motor_mode_extended_position(idd)
    gbl.dxl_motor.setProfileVelocityAcceleration(20, 0)


def handle_set_position(_):
    idd = int(gbl.dxl_id_choice.get())
    g = int(gbl.dxl_set_goal.get())
    gbl.dxl_motor.setGoalPosition(g)
    print(idd, g)


def handle_get_position(_):
    if gbl.dxl_motor is None:
        return
    if gbl.dxl_pos is None:
        return
    p, load, temp = gbl.dxl.get_multi_motor_position_info([gbl.dxl_motor])[gbl.dxl_motor._dxl_id]
    print("Pos: {} Load: {} Temp: {}".format(p, load, temp))
    # p = gbl.dxl_motor.presentPosition()
    # load = gbl.dxl_motor.presentLoadPercent()
    # temp = gbl.dxl_motor.presentTemperature()
    vol = gbl.dxl_motor.presentVoltage()
    status = "Pos: {} Load: {} Temp: {} Vol: {}".format(p, load, temp, vol)
    gbl.dxl_pos.delete(0, tk.END)
    gbl.dxl_pos.insert(0, status)
    # gbl.dxl_set_goal.set(p)
    print(status)


def handle_get_homing_offset(_):
    p = gbl.dxl_motor.get_homing_offset()
    gbl.dxl_homing_offset.set(p)


def handle_set_homing_offset(_):
    p = int(gbl.dxl_homing_offset.get())
    gbl.dxl_motor.set_homing_offset(p)


def create_ping_frame(window):
    ping_frame = tk.Frame(master=window)
    lbl_ping = tk.Label(master=ping_frame, text="Ping")
    btn_ping = tk.Button(ping_frame, text="Ping")
    btn_ping.bind("<Button-1>", functools.partial(handle_ping, lbl_ping))
    lbl_ping.pack(side=tk.LEFT)
    btn_ping.pack(side=tk.RIGHT)
    ping_frame.pack()


def create_rename_form(window):
    form_rename = tk.Frame(master=window)
    ent_from = tk.Entry(master=form_rename, width=2)
    ent_to = tk.Entry(master=form_rename, width=2)
    lbl_from = tk.Label(master=form_rename, text="From")
    lbl_to = tk.Label(master=form_rename, text="To")
    btn_rename = tk.Button(form_rename, text="Rename")
    lbl_from.grid(row=0, column=0, sticky="w")
    ent_from.grid(row=0, column=1, sticky="e")
    lbl_to.grid(row=0, column=2, sticky="w")
    ent_to.grid(row=0, column=3, sticky="e")
    btn_rename.grid(row=0, column=4, sticky="e")
    btn_rename.bind(
        "<Button-1>", functools.partial(handle_rename, ent_from, ent_to))
    form_rename.pack()


def create_position_form(window):
    form = tk.Frame(master=window)
    gbl.dxl_id_choice = ttk.Combobox(
        window, state="readonly", values=("1", "2", "3"))
    gbl.dxl_id_choice.pack()
    gbl.dxl_id_choice.current(0)
    gbl.dxl_id_choice.bind('<<ComboboxSelected>>', on_motor_id_selected)
    gbl.is_extended_mode = tk.IntVar()
    is_extended_mode_btn = tk.Checkbutton(window, text="Extended Mode",
                                          variable=gbl.is_extended_mode,
                                          onvalue=1,
                                          offvalue=0,
                                          command=on_motor_id_selected)
    is_extended_mode_btn.pack()
    gbl.dxl_pos = tk.Entry(master=form, width=60)
    gbl.dxl_pos.pack()
    btn = tk.Button(form, text="Get")
    btn.pack()
    btn.bind("<Button-1>", handle_get_position)
    gbl.dxl_set_goal = tk.Scale(
        form, from_=0, to=4096, orient=tk.HORIZONTAL, length=300)
    gbl.dxl_set_goal.pack()
    btn = tk.Button(form, text="Set")
    btn.pack()
    btn.bind("<Button-1>", handle_set_position)
    form.pack()


def create_homing_offset_form(window):
    form = tk.Frame(master=window)
    gbl.dxl_homing_offset = tk.Scale(
        form, from_=0, to=4096, orient=tk.HORIZONTAL, length=300)
    gbl.dxl_homing_offset.pack()
    btn = tk.Button(form, text="Get")
    btn.pack()
    btn.bind("<Button-1>", handle_get_homing_offset)
    btn = tk.Button(form, text="Set")
    btn.pack()
    btn.bind("<Button-1>", handle_set_homing_offset)
    form.pack()


def handle_move_until_load_hit(ent_load, ent_pos_inc, _):
    p = gbl.dxl_motor.presentPosition()
    for i in range(100):
        gbl.dxl_motor.setGoalPosition(p + i*10)
        while gbl.dxl_motor.isMoving():
            load = gbl.dxl_motor.presentLoadPercent()
            print('load', load, 'pos', gbl.dxl_motor.presentPosition())
            if abs(load) > 25:
                return
            time.sleep(0.01)
    # gbl.dxl_motor.moveUntilLoadHit(int(ent_load.get()), int(ent_pos_inc.get()))


def create_move_until_load_hit_form(window):
    form = tk.Frame(master=window)
    ent_load = tk.Entry(master=form, width=4)
    ent_load.pack()
    ent_pos_inc = tk.Entry(master=form, width=4)
    ent_pos_inc.pack()
    btn = tk.Button(form, text="Move till load")
    btn.pack()
    btn.bind(
        "<Button-1>", functools.partial(handle_move_until_load_hit, ent_load, ent_pos_inc))
    form.pack()


def loop():
    handle_get_position(None)
    window.after(1000, loop)


window = tk.Tk()
window.title("GUI DXL remote")
window.resizable(width=False, height=False)
create_ping_frame(window)
create_rename_form(window)
create_position_form(window)
create_homing_offset_form(window)
create_move_until_load_hit_form(window)
window.after(1000, loop)
window.mainloop()
