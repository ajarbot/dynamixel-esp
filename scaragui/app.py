import flask
import robot as robotapi
import cam_serve

app = flask.Flask(__name__, static_folder='static')
robot = robotapi.ScaraRobot("10.0.0.212")

_canvas_size = 500

def _to_screen_size(p):
    return (p[0]*0.75+_canvas_size, p[1]*0.75)

@app.route('/')
def index():
    return flask.send_from_directory('static', 'index.html')

@app.route('/move_actuator_positions', methods=['POST'])
def move_actuator_positions():
    numbers = flask.request.get_json()
    print(numbers)
    if len(numbers)==2:
        robot.move(x=(numbers[0]-_canvas_size)/0.75, y=numbers[1]/0.75, z=None)
        return flask.jsonify({'result': 'ok'})
    elif len(numbers)==1:
        robot.move(x=None, y=None, z=numbers[0])

@app.route('/get_actuator_positions', methods=['POST'])
def get_actuator_positions():
    xy1, xy2, z = robot.all_positions()
    return flask.jsonify([_to_screen_size(xy1), _to_screen_size(xy2), z])

@app.route('/video_feed.jpg')
def video_feed():
    return flask.Response(cam_serve.generate_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', threaded=True) 
