#!/bin/sh

g++ -std=c++11 arduino_command_unittest.cc -o arduino_command_unittest
./arduino_command_unittest

g++ -std=c++11 load_aware_goal_unittest.cc -o load_aware_goal_unittest
./load_aware_goal_unittest


export PYTHONPATH=$PYTHONPATH:$(pwd)/..

python3 dxl_motor_test.py
python3 dxl_remote_test.py
