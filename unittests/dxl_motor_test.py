from dxl.dxl_command import DxlCommand
from dxl.DxlMotor import DxlMotor as DxlMotor
import unittest


class DummyRemote:
    def __init__(self) -> None:
        self._resp = {DxlCommand.INIT_POSITION_MODE: 0}

    def _expect(self, cmd, resp):
        self._resp[cmd] = resp

    def _send(self, cmd, dxl_id, req=None, resp_size=1, resp_signed=False):
        if cmd == DxlCommand.GET_PRESENT_POSITION:
            return self._resp[cmd]
        elif cmd == DxlCommand.SET_GOAL_POSITION:
            return int.from_bytes(req, 'little')
        return None


class TestDxlMotor(unittest.TestCase):

    def setUp(self):
        pass

    def setCommandAndGetPresentPosition(self, motor, pos):
        motor._remote._expect(DxlCommand.GET_PRESENT_POSITION, pos)
        return motor.presentPosition()

    def setGoalAndExpectCommand(self, motor, pos, expect_command_pos):
        motor._remote._expect(
            DxlCommand.SET_GOAL_POSITION, expect_command_pos)
        self.assertEqual(expect_command_pos, motor.setGoalPosition(pos))

    def test_presentPosition(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1)
        self.assertEqual(100, self.setCommandAndGetPresentPosition(motor, 100))

    def test_presentPosition_start_offset(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1, [100, 4096])
        self.assertEqual(0, self.setCommandAndGetPresentPosition(motor, 100))
        self.assertEqual(100, self.setCommandAndGetPresentPosition(motor, 200))
        self.assertEqual(0, self.setCommandAndGetPresentPosition(motor, 0))
        self.assertEqual(0, self.setCommandAndGetPresentPosition(motor, -100))
        self.assertEqual(
            3995, self.setCommandAndGetPresentPosition(motor, 4095))
        self.assertEqual(
            3995, self.setCommandAndGetPresentPosition(motor, 4096))

    def test_presentPosition_start_end(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1, [1000, 3000])
        self.assertEqual(0, self.setCommandAndGetPresentPosition(motor, 1000))
        self.assertEqual(
            1000, self.setCommandAndGetPresentPosition(motor, 2000))
        self.assertEqual(
            1999, self.setCommandAndGetPresentPosition(motor, 2999))
        # self.assertEqual(2000, self.setCommandAndGetPresentPosition(motor, 3000))

    def test_presentPosition_start_end_out_of_range(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1, [1000, 3000])
        self.assertEqual(0, self.setCommandAndGetPresentPosition(motor, 0))

    def test_presentPosition_reverse(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1, [3000, 1000])
        # self.assertEqual(0, self.setCommandAndGetPresentPosition(motor, 3000))
        self.assertEqual(1, self.setCommandAndGetPresentPosition(motor, 2999))
        self.assertEqual(
            1000, self.setCommandAndGetPresentPosition(motor, 2000))
        self.assertEqual(
            1999, self.setCommandAndGetPresentPosition(motor, 1001))
        # self.assertEqual(2000, self.setCommandAndGetPresentPosition(motor, 1000))
        self.assertEqual(
            1500, self.setCommandAndGetPresentPosition(motor, 1500))

    def test_setGoal(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1)
        self.assertEqual(0, motor.setGoalPosition(0))
        self.assertEqual(100, motor.setGoalPosition(100))
        self.assertEqual(4095, motor.setGoalPosition(4095))
        self.assertEqual(4095, motor.setGoalPosition(4096))
        self.assertEqual(0, motor.setGoalPosition(-100))
        self.assertEqual(4095, motor.setGoalPosition(5000))

    def test_setGoal_start_end(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1, [1000, 3000])
        self.assertEqual(1000, motor.setGoalPosition(0))
        self.assertEqual(2999, motor.setGoalPosition(2000))
        self.assertEqual(2999, motor.setGoalPosition(2999))
        self.assertEqual(2999, motor.setGoalPosition(3000))
        self.assertEqual(2999, motor.setGoalPosition(4000))
        self.assertEqual(1000, motor.setGoalPosition(-100))

    def test_setGoal_reverse(self):
        remote = DummyRemote()
        motor = DxlMotor(remote, 1, [3000, 1000])
        self.assertEqual(3000, motor.setGoalPosition(0))
        self.assertEqual(2999, motor.setGoalPosition(1))
        self.assertEqual(1500, motor.setGoalPosition(1500))
        self.assertEqual(1001, motor.setGoalPosition(1999))
        self.assertEqual(1001, motor.setGoalPosition(2000))
        self.assertEqual(1001, motor.setGoalPosition(2999))
        self.assertEqual(1001, motor.setGoalPosition(3000))
        self.assertEqual(1001, motor.setGoalPosition(4000))
        self.assertEqual(3000, motor.setGoalPosition(-100))


if __name__ == '__main__':
    unittest.main()
