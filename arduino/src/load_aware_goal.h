#ifndef DYNAMIXEL_ESP_SRC_LOAD_AWARE_GOAL_H_
#define DYNAMIXEL_ESP_SRC_LOAD_AWARE_GOAL_H_

#include "dxl_util.h"
namespace load_aware_goal {

enum class State {
  kActiveMoving,
  kReachedEndGoal,
  kReachedLoadLimit,
  kReachedEndLoop,
};

struct {
  State result = State::kReachedEndGoal;
  byte dxl_id;
  int32_t end_goal;
  uint32_t remaining_loop;
  uint8_t sleep_ms;
  uint8_t load_max_percent;
  int8_t pos_inc;
  int32_t last_goal;
  unsigned long next_tick;

  void clear() {
    result = State::kReachedEndGoal;
    dxl_id = 0;
    end_goal = 0;
    remaining_loop = 0;
    sleep_ms = 0;
    load_max_percent = 0;
    pos_inc = 0;
    last_goal = 0;
    next_tick = 0;
  }
} state;

byte setUp(byte dxl_id, const byte *req, byte req_len) {
  if (req_len != 8) {
    return 0;
  }
  state.clear();
  state.dxl_id = dxl_id;
  memcpy(&state.end_goal, req, 4);
  state.remaining_loop = req[4] * 10;
  state.sleep_ms = req[5];
  state.load_max_percent = req[6];
  state.pos_inc = req[7];
  state.last_goal = dxl_util::getPresentPosition(dxl_id);
  if (state.last_goal > state.end_goal) {
    state.pos_inc = -state.pos_inc;
  }
  state.result = State::kActiveMoving;
  state.next_tick = millis();
  return 1;
}

byte getGoalStatus() { return static_cast<byte>(state.result); }

void loop() {
  if (state.result != State::kActiveMoving) {
    return;
  }
  if (millis() < state.next_tick) {
    return;
  }
  state.next_tick = millis() + state.sleep_ms;
  if (abs(presentLoadPercent(state.dxl_id)) > state.load_max_percent) {
    // We hit the load limit.
    state.result = State::kReachedLoadLimit;
    return;
  }
  if (dxl_util::movingStatus(state.dxl_id) & 0x2) {
    // We are still moving. Wait for the next loop.
    return;
  }
  if (state.remaining_loop == 0) {
    state.result = State::kReachedEndLoop;
    return;
  }
  if (state.last_goal == state.end_goal) {
    // Reached the end goal.
    state.result = State::kReachedEndGoal;
    return;
  }

  // Now we can move to next intermediate goal.
  state.remaining_loop--;
  state.last_goal = state.last_goal + state.pos_inc;
  if ((state.pos_inc > 0 && state.last_goal > state.end_goal) ||
      (state.pos_inc < 0 && state.last_goal < state.end_goal)) {
    // Reached very very close.
    state.last_goal = state.end_goal;
  }
  dxl_util::setGoalPosition(state.dxl_id, state.last_goal);
}

} // namespace load_aware_goal

#endif // DYNAMIXEL_ESP_SRC_LOAD_AWARE_GOAL_H_
