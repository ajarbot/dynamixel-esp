
byte fast_sync_read(uint16_t dxl_id_map, byte addr, byte addr_len, byte *resp,
                    byte resp_len) {
  // https://github.com/ROBOTIS-GIT/Dynamixel2Arduino/blob/master/examples/advanced/fast_sync_read/fast_sync_read.ino
  const byte MAX_DXL_ID_CNT = 8;
  DYNAMIXEL::InfoSyncReadInst_t infos_sync_read;
  DYNAMIXEL::XELInfoSyncRead_t info_sync_read[MAX_DXL_ID_CNT];
  byte user_pkt_buf[128];

  infos_sync_read.packet.p_buf = user_pkt_buf;
  infos_sync_read.packet.buf_capacity = sizeof(user_pkt_buf);
  infos_sync_read.packet.is_completed = false;
  infos_sync_read.addr = addr;
  infos_sync_read.addr_length = addr_len;
  infos_sync_read.p_xels = info_sync_read;
  infos_sync_read.is_info_changed = true;
  infos_sync_read.xel_count = 0;

  for (byte i = 0; i < MAX_DXL_ID_CNT; i++) {
    if (dxl_id_map & (1 << i)) {
      DEBUG_SERIAL.print("id ");
      DEBUG_SERIAL.print(i, HEX);
      info_sync_read[i].id = i;
      info_sync_read[i].p_recv_buf = resp + (infos_sync_read.xel_count * addr_len);
      infos_sync_read.xel_count++;
      if (infos_sync_read.xel_count == MAX_DXL_ID_CNT) {
        return 0;
      }
      if (resp_len < (infos_sync_read.xel_count * addr_len)) {
        return 0;
      }
    }
  }
  byte res = dxl_util::dxl.fastSyncRead(&infos_sync_read, 100);
  DEBUG_SERIAL.print("res ");
  DEBUG_SERIAL.println(res, HEX);
  if (res ==0) {
    DEBUG_SERIAL.print("err ");
    DEBUG_SERIAL.println(dxl_util::dxl.getLastLibErrCode());
  }
}
