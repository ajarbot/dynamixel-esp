const canvas = document.getElementById('robotCanvas');
const ctx = canvas.getContext('2d');

// SCARA arm parameters
const link1Length = 100;
const link2Length = 80;

function fetchJSON(url, param) {
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(param)
  })
  .then(response => response.json())
}

function drawArm(xy1, xy2) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.lineWidth = 10;

    const baseX =  canvas.width / 2;
    ctx.fillStyle = "blue";
    ctx.beginPath();
    // ctx.arc(baseX, 100, 100, 0, 2 * Math.PI); // Circle
    ctx.fill();
    ctx.fillStyle = "orange";

    ctx.beginPath();
    ctx.strokeRect(baseX-10, -10, 20, 20); // Circle
    ctx.arc(baseX, 0, 15, 0, 2 * Math.PI); // Circle
    ctx.fill();

    // Draw the arm
    ctx.beginPath();
    ctx.moveTo(baseX, 0);
    ctx.lineTo(xy1[0], xy1[1]);
    ctx.lineTo(xy2[0], xy2[1]);
    ctx.stroke();

    // Draw a marker for the end-effector
    ctx.beginPath();
    ctx.arc(xy1[0], xy1[1], 20, 0, 2 * Math.PI); // Circle
    ctx.fill();
    ctx.arc(xy2[0], xy2[1], 20, 0, 2 * Math.PI); // Circle
    ctx.fill();
}

let angle1 = 0;
let angle2 = Math.PI / 4;
setInterval(() => {
  fetchJSON('/get_actuator_positions').then(([xy1,xy2,z]) => {
    drawArm(xy1,xy2);
  });
}, 1000); 

canvas.addEventListener('click', function(event) {
  fetchJSON('/move_actuator_positions', [event.offsetX,event.offsetY]);
});
