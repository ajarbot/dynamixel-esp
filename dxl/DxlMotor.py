from dxl.dxl_command import DxlCommand, DxlExtended
import struct


class DxlMotor:
    def __init__(self, remote, dxl_id, range=[0, 4096]) -> None:
        self._remote = remote
        self._dxl_id = dxl_id
        self._range = range
        self._range_diff = abs(self._range[0]-self._range[1])
        self._range_min = min(self._range)
        self._range_max = max(self._range)
        self._reverse = range[0] > range[1]
        remote._send(DxlCommand.INIT_POSITION_MODE, dxl_id)

    def _set_goal_position(self, pos):
        return self._remote._send(DxlCommand.SET_GOAL_POSITION,
                                  self._dxl_id,
                                  req=pos.to_bytes(4, "little", signed=True))

    def _set_goal_position3(self, pos):
        req = struct.pack('<iBBBB', pos,
                          200,  # loop
                          50,  # sleep
                          30,  # load max
                          10)
        return self._remote._send(DxlCommand.LOAD_AWARE_GOAL_SET,
                                  self._dxl_id, req)

    def _present_position_raw(self):
        return self._remote._send(DxlCommand.GET_PRESENT_POSITION,
                                  self._dxl_id,
                                  resp_size=4, resp_signed=True)

    def setProfileVelocityAcceleration(self, velocity, acceleration):
        return self._remote._send(DxlExtended.SET_PROFILE_VELOCITY_ACCELERATION,
                                  self._dxl_id,
                                  req=b''.join([velocity.to_bytes(2, 'little'),
                                                acceleration.to_bytes(2, 'little')]))

    def setGoalPosition(self, pos):
        if pos < 0:
            pos = 0
        if pos >= self._range_diff:
            pos = self._range_diff-1
        if self._reverse:
            pos = self._range[0]-pos
        else:
            pos += self._range[0]
        return self._set_goal_position(pos)

    def computePresentPosition(self, raw_pos):
        if raw_pos == None:
            return None
        if raw_pos < self._range_min:
            raw_pos = self._range_min
        if raw_pos >= self._range_max:
            raw_pos = self._range_max-1
        if self._reverse:
            raw_pos = -raw_pos
            pass
        return (raw_pos-self._range[0]) % self._range_diff

    def presentPosition(self):
        return self.computePresentPosition(self._present_position_raw())

    def moving_status(self):
        return self._remote._send(DxlExtended.MOVING_STATUS,
                                  self._dxl_id)

    def get_homing_offset(self):
        return self._remote._send(DxlExtended.GET_HOMING_OFFSET,
                                  self._dxl_id,
                                  resp_size=4, resp_signed=True)

    def set_homing_offset(self, offset):
        return self._remote._send(DxlExtended.SET_HOMING_OFFSET,
                                  self._dxl_id,
                                  req=offset.to_bytes(4, "little", signed=True))

    def presentLoadPercent(self):
        return self._remote._send(DxlExtended.PRESENT_LOAD_PERCENT,
                                  self._dxl_id,
                                  resp_size=1, resp_signed=True)

    def presentTemperature(self):
        return self._remote._send(DxlExtended.PRESENT_TEMPERATURE,
                                  self._dxl_id)

    def presentVoltage(self):
        return self._remote._send(DxlExtended.PRESENT_VOLTAGE,
                                  self._dxl_id,
                                  resp_size=4, resp_signed=True)

    def hardwareErrorStatus(self):
        return self._remote._send(DxlExtended.HARDWARE_ERROR_STATUS,
                                  self._dxl_id,
                                  resp_size=4, resp_signed=True)

    def isMoving(self):
        m = self.moving_status()
        if m == None:
            return False
        return m & 0x2

    def getModelInfo(self):
        return struct.unpack('<IHBB', self._remote._send(DxlExtended.GET_MODEL_INFO,
                                                         self._dxl_id,
                                                         resp_size=8))

    def _read_control_table(self, addr, addr_len):
        if addr_len > 4:
            return None
        return self._remote._send(DxlCommand.READ_CONTROL_TABLE,
                                  self._dxl_id,
                                  req=struct.pack('BB', addr, addr_len),
                                  resp_size=addr_len)

    def _write_control_table(self, addr, addr_len, data):
        if addr_len < 4:
            return None
        req = bytearray()
        req.append(addr)
        req.append(addr_len)
        req.extend(data.to_bytes(addr_len, "little"))
        return self._remote._send(DxlCommand.WRITE_CONTROL_TABLE,
                                  self._dxl_id,
                                  req=req,
                                  resp_size=addr_len)
