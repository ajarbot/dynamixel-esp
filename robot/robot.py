# import dxlremote
import robot.dxl_dummy as dxlremote
import time
import math


# https://stackoverflow.com/questions/76970645/calculate-inverse-kinematics-of-2-dof-planar-robot-in-python
# https://www.youtube.com/watch?v=jyQyusoxlW8
def _scara_inverse_kinematics(x, y, a1, a2):
    """Calculates the joint angles (theta1, theta2) for a SCARA robot.
    Args:
        x (float): Desired X coordinate of the end-effector.
        y (float): Desired Y coordinate of the end-effector.
        a1 (float): Length of the first arm.
        a2 (float): Length of the second arm.
    Returns:
        tuple: Two possible solutions: 
               * (theta1_1, theta2_1), 
               * (theta1_2, theta2_2)
               If the position is not reachable, returns None.
    """
    r2 = x**2 + y**2
    c2 = (a1*a1 + a2*a2 - r2) / (2 * a1 * a2)
    theta2 = math.pi - math.acos(c2)

    k = (r2 + a1*a1 - a2*a2)/ (2*math.sqrt(r2)*a1)
    if k > 1 or k < -1:
        return None
    k = math.acos(k)

    theta1_1 = math.atan2(y, x) - k
    theta1_2 = math.atan2(y, x) + k
    return (theta1_1, theta2), (theta1_2, -theta2)


class Actuator:
    def __init__(self, dxl, idd, turns=1, home=0, speed=20) -> None:
        self.turns = turns
        if turns == 1:
            self._motor = dxl.motor_mode_position(idd)
            # self._motor.set_homing_offset(home)
        else:
            self._motor = dxl.motor_mode_extended_position(idd)
        self._motor.set_velocity(speed)

    def radians(self):
        pos = self._motor.presentPosition()
        if pos == None:
            return None
        if pos > self.turns * 4096:
            pos = self.turns * 4096
        return 2 * math.pi * pos / (self.turns * 4096)

    def move(self, radians):
        pos = int(radians * (self.turns * 4096) / (2 * math.pi))
        self._motor.setGoalPosition(pos)


class ScaraRobot:
    def __init__(self, ip="10.0.0.212") -> None:
        self._dxl = dxlremote.DxlRemote(ip)
        self.mxy1 = Actuator(self._dxl, 6, turns=3)
        self.mxy2 = Actuator(self._dxl, 4)
        self.mz = Actuator(self._dxl, 5, turns=3.0/40, speed=100)
        self.arm1 = 340
        self.arm2 = 300

    def position(self):
        a1 = self.mxy1.radians()
        a2 = self.mxy2.radians()
        x = self.arm1 * math.cos(a1) + self.arm2 * math.cos(a1 + a2)
        y = self.arm1 * math.sin(a1) + self.arm2 * math.sin(a1 + a2)
        return x, y

    def all_positions(self):
        a1 = self.mxy1.radians()
        a2 = self.mxy2.radians()
        z = self.mz.radians()
        x = self.arm1 * math.cos(a1) + self.arm2 * math.cos(a1 + a2)
        y = self.arm1 * math.sin(a1) + self.arm2 * math.sin(a1 + a2)
        print(x,y)
        return [self.arm1 * math.cos(a1), self.arm1 * math.sin(a1)], [x, y], z

    def _forward(self, a1, a2):
        x = self.arm1 * math.cos(a1) + self.arm2 * math.cos(a1 + a2)
        y = self.arm1 * math.sin(a1) + self.arm2 * math.sin(a1 + a2)
        return [x, y]

    def move(self, x, y, z):
        if z != None:
            self.mz._motor.move(z)
        if x == None or y == None:
            return False
        answers = _scara_inverse_kinematics(x, y, self.arm1, self.arm2)
        if answers == None:
            print("Position not reachable")
            return False
        self.mxy1.move(answers[0][0])
        self.mxy2.move(answers[0][1])
        print(x,y)
        print(answers)
        print(self._forward(*answers[0]))
        print(self._forward(*answers[1]))
        return True
