class DxlMotor:
    def __init__(self, remote, dxl_id, range=[0, 4096]) -> None:
        self._remote = remote
        self._dxl_id = dxl_id
        self._goal = 0
        self._pos = 0

    def setGoalPosition(self, pos):
        self._goal = pos
        self._pos = pos
        print('set', pos)

    def presentPosition(self):
        return self._pos

    def set_velocity(self, vel):
        pass


class DxlRemote:
    def __init__(self, ip) -> None:
        pass

    def motor_mode_position(self, dxl_id):
        return DxlMotor(self, dxl_id)

    def motor_mode_extended_position(self, dxl_id):
        return DxlMotor(self, dxl_id)
