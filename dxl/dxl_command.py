from enum import IntEnum


class DxlCommand(IntEnum):
    INIT_POSITION_MODE = 0
    INIT_EXTENDED_POSITION_MODE = 1
    PING = 2
    READ_CONTROL_TABLE = 3
    WRITE_CONTROL_TABLE = 4
    READ_MULTI = 5
    SET_GOAL_POSITION = 6
    GET_PRESENT_POSITION = 7
    LOAD_AWARE_GOAL_SET = 9
    LOAD_AWARE_GOAL_STATUS = 10
    FAST_SYNC_READ_MULTI = 0xE
    DXL_EXTENDED_COMMAND = 0xF

    def get_dxl_bits(self, dxl_id):
        return ((self.value << 4) | (dxl_id & 0xF), )


class DxlReadMultiCommand(IntEnum):
    READ_CONTROL_TABLE = 0
    GET_POSITION_LOAD_TEMPERATURE = 1

    def get_dxl_bits(self, dxl_id):
        return ((DxlCommand.READ_MULTI.value << 4) | (self.value & 0xF), )


class DxlExtended(IntEnum):
    SET_ID = 0
    GET_HOMING_OFSET = 1
    SET_HOMING_OFFSET = 2
    SET_PROFILE_VELOCITY_ACCELERATION = 3
    MOVING_STATUS = 4
    PRESENT_LOAD_PERCENT = 5
    PRESENT_TEMPERATURE = 6
    PRESENT_VOLTAGE = 7
    HARDWARE_ERROR_STATUS = 8
    GET_MODEL_INFO = 9

    def get_dxl_bits(self, dxl_id):
        return (0xF0 | (dxl_id & 0xF), self.value)
