#include <ESP8266WiFi.h>
#include <Wire.h>

constexpr byte SLAVE_ADDRESS = 0x3a;
constexpr uint16_t SERVER_PORT = 0x3a3a;

WiFiServer server(SERVER_PORT);
WiFiClient client;

void setup() {
  Serial.begin(115200);
  WiFi.begin("Ajarirus", "SeattleBallard");

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("WiFi connected");
  Serial.println(WiFi.localIP());

  Wire.begin();
  server.begin();
}

bool sock_read(WiFiClient *client, byte *buf, byte size) {
  for (byte i = 0; i < size; i++) {
    if (!client->available())
      return false;
    int b = client->read();
    if (b == -1)
      return false;
    buf[i] = b;
  }
  return true;
}

bool wire_read(byte *buf, byte size) {
  for (byte i = 0; i < size; i++) {
    if (!Wire.available())
      return false;
    int b = Wire.read();
    if (b == -1)
      return false;
    buf[i] = b;
  }
  return true;
}

bool check_xor_sum(byte *buf, byte size) {
  byte sum = 0x4A; // Just a random number used in both master and slave.
  for (byte i = 1; i < size; i++) {
    sum ^= buf[i];
  }
  return sum == buf[0];
}

bool handleOneRequest() {
  if (!client.available())
    return true;
  byte reqsize, respsize;
  if (!sock_read(&client, &reqsize, 1))
    return false;
  if (!sock_read(&client, &respsize, 1))
    return false;
  byte req[64], resp[128];
  if (reqsize <= 0 || reqsize > sizeof(req))
    return false;
  if (respsize <= 0 || respsize > sizeof(resp))
    return false;
  if (!sock_read(&client, req, reqsize))
    return false;

  Serial.print("req ");
  Serial.print(reqsize, HEX);
  Wire.beginTransmission(SLAVE_ADDRESS);
  Wire.write(req, reqsize);
  Wire.endTransmission();
  unsigned long start = millis();
  bool is_valid_packet = false;
  do {
    delay(3);
    Wire.requestFrom(SLAVE_ADDRESS, respsize + 1U);
    delay(2);
    if (!wire_read(resp, respsize + 1U)) {
      return false;
    }
    if (check_xor_sum(resp, respsize + 1U)) {
      // Received a valid packet.
      is_valid_packet = true;
      break;
    }
  } while (millis() - start < 300);
  if (!is_valid_packet) {
    Serial.print("timeout");
    client.stop();
    return false;
  }

  for (byte i = 0; i < respsize; i++) {
    client.write(resp[i + 1]);
  }
  client.flush();
  return true;
}

void loop() {
  delay(10);
  if (!client || !client.connected()) {
    client = server.available();
    if (!client)
      return;
    client.setNoDelay(true);
  }
  if (!handleOneRequest()) {
    // client.stop();
  }
}
