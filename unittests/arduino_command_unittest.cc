#include <cassert>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <map>

typedef uint8_t byte;

#include "../arduino/src/all_decl.h"
#include "../arduino/src/common.h"

#define assertEquals(expected, actual)                                         \
  do {                                                                         \
    auto actual_ = actual;                                                     \
    if (expected != actual_) {                                                 \
      std::cout << "Expected " << expected << ", got " << int(actual_)         \
                << std::endl;                                                  \
    }                                                                          \
    assert(expected == actual_);                                               \
  } while (0)

struct TestDxl {
  std::map<int, int32_t> goal_position;
  std::map<int, int32_t> present_position;
  std::map<int, int32_t> load;
  std::map<int, int32_t> temperature;
} test;

byte _cmd(DxlCommand cmd, uint8_t id) { return int(cmd) << 4 | id; }

namespace dxl_util {

void init() {}
bool setProfileVelocityAcceleration(uint8_t id, uint32_t velocity,
                                    uint32_t acceleration) {
  return true;
}
bool positionMode(uint8_t id) { return true; }
bool extendedPositionMode(uint8_t id) { return true; }
bool setGoalPosition(uint8_t id, int32_t pos) {
  test.goal_position[id] = pos;
  return true;
}
int32_t getPresentPosition(uint8_t id) { return test.present_position[id]; }
bool ping(uint8_t id) { return true; }
bool setID(uint8_t id, uint8_t new_id) { return true; }
int32_t moving(uint8_t id) { return 0; }
uint32_t movingStatus(uint8_t id) { return 0; }
uint32_t getHomingOffset(uint8_t id) { return 0; }
bool setHomingOffset(uint8_t id, uint32_t offset) { return 0; }
int32_t presentLoad(uint8_t id) { return test.load[id]; }
int32_t presentTemperature(uint8_t id) { return test.temperature[id]; }
int32_t presentVoltage(uint8_t id) { return 0; }
int32_t hardwareErrorStatus(uint8_t id) { return 0; }
ModelInfo getModelInfo(uint8_t id) { return ModelInfo(); }

int32_t read(uint8_t id, uint8_t addr, uint8_t addr_length, uint8_t *resp,
             uint8_t resp_len) {
  return 0;
}
bool write(uint8_t id, uint8_t addr, const uint8_t *req, uint8_t req_len) {
  return 0;
}

} // namespace dxl_util

namespace load_aware_goal {
byte setUp(byte dxl_id, const byte *req, byte req_len) { return 0; }
byte getGoalStatus() { return 0; }
void loop() {}
} // namespace load_aware_goal

byte fast_sync_read(uint16_t dxl_id_map, byte addr, byte len, byte *resp,
                    byte resp_len) {
  return 0;
}

void test_get_presentPosition() {
  byte req[100], resp[100];
  req[0] = _cmd(DxlCommand::GET_PRESENT_POSITION, 2);
  test.present_position[2] = 1234;
  assertEquals(4, handleCommand(req, 1, resp, 100));
  assertEquals(1234, *(int32_t *)(resp));
}

void test_set_goal() {
  byte req[100], resp[100];
  req[0] = _cmd(DxlCommand::SET_GOAL_POSITION, 3);
  *(int32_t *)(req + 1) = 1234;
  assertEquals(1, handleCommand(req, 5, resp, 100));
  assertEquals(1234, test.goal_position[3]);
}

void test_get_position_load_temperature_multi() {
  byte req[100], resp[100];
  req[0] = _cmd(DxlCommand::READ_MULTI,
                static_cast<byte>(DxlReadMultiCommand::GET_POSITION_LOAD_TEMPERATURE));
  *(uint16_t *)(req + 1) = 0x409; // bits 0, 3, 10
  test.present_position[0] = 1234;
  test.present_position[3] = 1235;
  test.present_position[10] = 1236;
  test.load[0] = 123;
  test.load[3] = -134;
  test.load[10] = 146;
  test.temperature[0] = 23;
  test.temperature[3] = 34;
  test.temperature[10] = -46;
  assertEquals(18, handleCommand(req, 3, resp, 100));
  assertEquals(1234, *(int32_t *)(resp));
  assertEquals(1235, *(int32_t *)(resp + 6));
  assertEquals(1236, *(int32_t *)(resp + 12));
  assertEquals(12, resp[4]);
  assertEquals(0x100 - 13, resp[10]);
  assertEquals(15, resp[16]);
  assertEquals(23, resp[5]);
  assertEquals(34, resp[11]);
  assertEquals(0x100 - 46, resp[17]);
}

int main() {
  test_get_presentPosition();
  test_set_goal();
  test_get_position_load_temperature_multi();
}
