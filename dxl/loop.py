import dxlremote
import time


def reachGoal(m,  goal):
    print(goal, m.setGoalPosition(goal))
    while True:
        print('move', goal, m.presentPosition(), m.presentLoadPercent())
        if not m.isMoving():
            break
        time.sleep(1)

def moveUntilLoadHit(m):
    p = m.presentPosition()
    for i in range(100):
        m.setGoalPosition(p + i*10)
        while True:
            time.sleep(0.01)
            load = m.presentLoadPercent()
            print('load', load, 'pos', m.presentPosition())
            if abs(load) > 25:
                return
            if not m.isMoving():
                break

dxl = dxlremote.DxlRemote("10.0.0.212")


def one():
    m1 = dxl.motor_mode_position(1)
    m2 = dxl.motor_mode_position(2)
    m3 = dxl.motor_mode_position(3)
    # m1  claw  full-open 1900
    # m2  top=2500 to bottom=
    while True:
        reachGoal(m1, 1900)
        reachGoal(m2, 2500)
        time.sleep(1)
        reachGoal(m2, 680)
        moveUntilLoadHit(m1)
        time.sleep(1)
        reachGoal(m2, 2500)
        time.sleep(5)
        reachGoal(m2, 680)
        reachGoal(m1, 1900)
        time.sleep(1)

def two():
    m5 = dxl.motor_mode_extended_position(5)
    m5.setProfileVelocityAcceleration(60,0)
    while True:
        print(m5.presentPosition())
        reachGoal(m5, -16000)
        time.sleep(1)
        reachGoal(m5, -32000)

def three():
    m2 = dxl.motor_mode_position(2)
    while True:
        # p, load, temp = dxl.get_multi_motor_position_info([m2])[m2._dxl_id]
        m2.setGoalPosition(1000)
        # v = m2.presentVoltage()
        # print(p, load, temp, v)
        # print(m2.getModelInfo())
        # print(m2._read_control_table(0, 2))
        # print(m2._read_control_table(146, 1))
        # print(m2._read_control_table(116, 4))
        # print(dxl._read_multi([m2], 116, 4))
        time.sleep(5)
    
two()
