import socket
import struct
import time
from dxl.DxlMotor import DxlMotor
from dxl.dxl_command import DxlCommand, DxlReadMultiCommand
import dxl.DxlMotorExtended

SERVER_PORT = 0x3a3a


class DxlRemote:
    def __init__(self, ip) -> None:
        self._reconnect(ip)

    def _reconnect(self, ip):
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect((ip, SERVER_PORT))

    def _send(self, cmd, dxl_id, req=None, resp_size=1, resp_signed=False, to_int=True):
        req_len = len(req) if req else 0
        cmd_bits = cmd.get_dxl_bits(dxl_id)
        r = bytearray((req_len + len(cmd_bits), resp_size))
        r.extend(cmd_bits)
        if req:
            r.extend(req)
        self._sock.send(r)
        print('req',r.hex(), cmd)
        start_time = time.time()
        resp = bytearray()
        while start_time + 2 >= time.time() and len(resp) < resp_size:
            resp.extend(self._sock.recv(resp_size-len(resp)))
            time.sleep(0.01)
        if not resp:
            return None
        print('resp',resp.hex())
        if to_int and resp_size <= 4:
            return int.from_bytes(resp, "little", signed=resp_signed)
        return resp

    def motor_mode_position(self, dxl_id):
        return DxlMotor(self, dxl_id)

    def motor_mode_extended_position(self, dxl_id):
        return dxl.DxlMotorExtended.DxlMotorExtendedRaw(self, dxl_id)

    def ping(self, dxl_id):
        return self._send(DxlCommand.PING, dxl_id)

    def _get_multi_command(self, cmd, dxl_ids, resp_len, req):
        dxl_id_map = 0
        for m in dxl_ids:
            assert m._dxl_id < 16
            dxl_id_map |= (1 << m._dxl_id)
        r = bytearray(dxl_id_map.to_bytes(2, 'little'))
        if req:
            r.extend(req)
        resp = self._send(cmd,
                          dxl_id=0,
                          req=r, resp_size=resp_len*len(dxl_ids), to_int=False)
        all_info = {}
        for i, m in enumerate(dxl_ids):
            all_info[m._dxl_id] = resp[0:6]
            del resp[0:6]
        return all_info

    def get_multi_motor_position_info(self, dxl_ids):
        all_motors = {}
        for m in dxl_ids:
            all_motors[m._dxl_id] = m
        all_info = self._get_multi_command(
            DxlReadMultiCommand.GET_POSITION_LOAD_TEMPERATURE, dxl_ids, 6, req=None)
        for dxl_id in all_info:
            v = all_info[dxl_id]
            v = list(struct.unpack('<ibb', v))
            v[0] = all_motors[dxl_id].computePresentPosition(v[0])
            all_info[dxl_id] = v
        return all_info

    def _read_multi(self, dxl_ids, addr, addr_len):
        all_motors = {}
        for m in dxl_ids:
            all_motors[m._dxl_id] = m
        all_info = self._get_multi_command(
            DxlCommand.READ_CONTROL_TABLE_MULTI, dxl_ids, addr_len, req=[addr, addr_len])
        for dxl_id in all_info:
            all_info[dxl_id] = int.from_bytes(all_info[dxl_id], 'little')
        return all_info

    # Not working yet.
    def _fast_sync_read_multi(self, dxl_ids, addr, addr_len):
        if addr_len > 4:
            return None
        all_info = self._get_multi_command(
            DxlCommand.FAST_SYNC_READ_MULTI, dxl_ids, addr_len, req=[addr, addr_len])
        for dxl_id in all_info:
            all_info[dxl_id] = int.from_bytes(all_info[dxl_id], 'little')
        return all_info


def reachGoal(m,  goal):
    print(m.set_goal_position(goal))
    while abs(goal-m.present_position()) > 10:
        time.sleep(1)
