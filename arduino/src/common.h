#ifndef DYNAMIXEL_ESP_SRC_COMMON_H_
#define DYNAMIXEL_ESP_SRC_COMMON_H_

#include "dxl_util.h"
int8_t presentLoadPercent(byte dxl_id) {
  return static_cast<int8_t>(round(dxl_util::presentLoad(dxl_id) / 10.0));
}

byte handleReadMultiCommand(uint16_t dxl_bits, DxlReadMultiCommand cmd,
                            const byte *req, byte req_len, byte *resp,
                            byte resp_len);

byte handleExtendedCommand(DxlExtendedCommand cmd, byte dxl_id, const byte *req,
                           byte req_len, byte *resp, byte resp_len);

byte fast_sync_read(uint16_t dxl_id_map, byte addr, byte len, byte *resp,
                    byte resp_len);

byte handleCommand(const byte *req, byte req_len, byte *resp, byte resp_len) {
  if (req_len < 1 || resp_len < 1) return 0;
  byte dxl_id = req[0] & 0xF;
  DxlCommand dxl_cmd = static_cast<DxlCommand>((req[0] & 0xF0) >> 4);
  switch (dxl_cmd) {
  case DxlCommand::INIT_POSITION_MODE:
    *resp = dxl_util::positionMode(dxl_id);
    return 1;
  case DxlCommand::INIT_EXTENDED_POSITION_MODE:
    *resp = dxl_util::extendedPositionMode(dxl_id);
    return 1;
  case DxlCommand::PING:
    *resp = dxl_util::ping(dxl_id);
    return 1;
  case DxlCommand::READ_CONTROL_TABLE: {
    if (req_len != 3)
      return 0;
    if (req[2] > 4 || req[2] > resp_len) {
      // Limit bytes read.
      return 0;
    }
    auto ret = dxl_util::read(dxl_id, req[1], req[2], resp, req[2]);
    if (ret == -1)
      return 0;
    return req[2];
  }
  case DxlCommand::WRITE_CONTROL_TABLE: {
    if (req_len < 4 || req_len > 7)
      return 0;
    *resp = dxl_util::write(dxl_id, req[1], req + 3, req[2]);
    return 1;
  }
  case DxlCommand::GET_PRESENT_POSITION: {
    if (resp_len< 4) return 0;
    int32_t pos = dxl_util::getPresentPosition(dxl_id);
    memcpy(resp, &pos, 4);
    return 4;
  }
  case DxlCommand::SET_GOAL_POSITION: {
    if (req_len != 5) return 0;
    int32_t val;
    memcpy(&val, req + 1, 4);
    *resp = dxl_util::setGoalPosition(dxl_id, val);
    return 1;
  }
  case DxlCommand::FAST_SYNC_READ_MULTI: {
    // DEBUG_SERIAL.print("fast ");
    if (req_len != 5)
      return 0;
    uint16_t dxl_bits;
    memcpy(&dxl_bits, req + 1, 2);
    // DEBUG_SERIAL.println(dxl_bits, HEX);
    // DEBUG_SERIAL.println(req[3], HEX);
    // DEBUG_SERIAL.println(req[4], HEX);
    return fast_sync_read(dxl_bits, req[3], req[4], resp, resp_len);
  }
  case DxlCommand::DXL_EXTENDED_COMMAND: {
    if (req_len < 2) return 0;
    return handleExtendedCommand(static_cast<DxlExtendedCommand>(req[1]),
                                 dxl_id, req + 2, req_len - 2, resp, resp_len);
  }
  case DxlCommand::LOAD_AWARE_GOAL_SET: {
    *resp = load_aware_goal::setUp(dxl_id, req + 1, req_len - 1);
    return 1;
  }
  case DxlCommand::LOAD_AWARE_GOAL_STATUS: {
    *resp = load_aware_goal::getGoalStatus();
    return 1;
  }
  case DxlCommand::READ_MULTI: {
    if (req_len < 3) {
      return 0;
    }
    uint16_t dxl_bits;
    memcpy(&dxl_bits, req + 1, 2);
    return handleReadMultiCommand(dxl_bits, static_cast<DxlReadMultiCommand>(dxl_id),
                           req + 3, req_len - 3, resp, resp_len);
  }
  }
  return 0;
}

byte handleReadMultiCommand(uint16_t dxl_bits, DxlReadMultiCommand cmd,
                            const byte *req, byte req_len, byte *resp,
                            byte resp_len) {
  uint8_t per_len = 0, num_dxls = 0;
  for (byte id = 0; id < 16; id++) {
    if (dxl_bits & (1 << id)) {
      num_dxls++;
    }
  }
  switch (cmd) {
  case DxlReadMultiCommand::READ_CONTROL_TABLE: {
    if (req_len != 2)
      return 0;
    per_len = req[1];
    break;
  }
  case DxlReadMultiCommand::GET_POSITION_LOAD_TEMPERATURE: {
    per_len = 6;
    break;
  }
  }
  if (num_dxls * per_len >= resp_len) {
    return 0;
  }

  uint8_t resp_end = 0;
  for (byte id = 0; id < 16; id++) {
    if (dxl_bits & (1 << id)) {
      switch (cmd) {
      case DxlReadMultiCommand::READ_CONTROL_TABLE: {
        auto ret =
            dxl_util::read(id, req[0], per_len, resp + resp_end, per_len);
        if (ret == -1) {
          // DEBUG_SERIAL.println("fail read");
          return 0;
        }
        break;
      }
      case DxlReadMultiCommand::GET_POSITION_LOAD_TEMPERATURE: {
        int32_t pos = dxl_util::getPresentPosition(id);
        byte load = static_cast<byte>(presentLoadPercent(id));
        byte temp = static_cast<byte>(dxl_util::presentTemperature(id));
        memcpy(resp + resp_end, &pos, 4);
        memcpy(resp + resp_end + 4, &load, 1);
        memcpy(resp + resp_end + 5, &temp, 1);
        break;
      }
      }
      resp_end += per_len;
    }
  }
  return resp_end;
}

byte handleExtendedCommand(DxlExtendedCommand cmd, byte dxl_id, const byte *req,
                           byte req_len, byte *resp, byte resp_len) {
  switch (cmd) {
  case DxlExtendedCommand::SET_ID: {
    if (req_len != 1) return 0;
    *resp = dxl_util::setID(dxl_id, req[0]);
    return 1;
  }
  case DxlExtendedCommand::GET_HOMING_OFSET: {
    if (resp_len <4) return 0;
    int32_t pos = dxl_util::getHomingOffset(dxl_id);
    memcpy(resp, &pos, 4);
    return 4;
  }
  case DxlExtendedCommand::SET_HOMING_OFFSET: {
    if (req_len != 4) return 0;
    int32_t pos;
    memcpy(&pos, req, 4);
    *resp = dxl_util::setHomingOffset(dxl_id, pos);
    return 1;
  }
  case DxlExtendedCommand::SET_PROFILE_VELOCITY_ACCELERATION: {
    if (req_len != 4) return 0;
    uint16_t profile_velocity;
    uint16_t profile_acceleration;
    memcpy(&profile_velocity, req, 2);
    memcpy(&profile_acceleration, req + 2, 2);
    *resp = dxl_util::setProfileVelocityAcceleration(dxl_id, profile_velocity,
                                                     profile_acceleration);
    return 1;
  }
  case DxlExtendedCommand::MOVING_STATUS:
    *resp = dxl_util::movingStatus(dxl_id);
    return 1;
  case DxlExtendedCommand::PRESENT_LOAD_PERCENT: {
    *resp = presentLoadPercent(dxl_id);
    return 1;
  }
  case DxlExtendedCommand::PRESENT_TEMPERATURE: {
    uint8_t temp = dxl_util::presentTemperature(dxl_id);
    *resp = temp;
    return 1;
  }
  case DxlExtendedCommand::PRESENT_VOLTAGE: {
    if (resp_len <4) return 0;
    int32_t pos = dxl_util::presentVoltage(dxl_id);
    memcpy(resp, &pos, 4);
    return 4;
  }
  case DxlExtendedCommand::HARDWARE_ERROR_STATUS: {
    if (resp_len <4) return 0;
    int32_t pos = dxl_util::hardwareErrorStatus(dxl_id);
    memcpy(resp, &pos, 4);
    return 4;
  }
  case DxlExtendedCommand::GET_MODEL_INFO: {
    if (resp_len < sizeof(dxl_util::ModelInfo))
      return 0;
    dxl_util::ModelInfo info = dxl_util::getModelInfo(dxl_id);
    memcpy(resp, &info, sizeof(dxl_util::ModelInfo));
    return sizeof(dxl_util::ModelInfo);
  }
  }
  return 0;
}

#endif // DYNAMIXEL_ESP_SRC_COMMON_H_
