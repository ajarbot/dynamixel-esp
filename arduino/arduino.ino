#include <Dynamixel2Arduino.h>
#include <Wire.h>

#include "src/all_decl.h"
#include "src/dxl_util.h"
#include "src/common.h"
#include "src/load_aware_goal.h"
#include "src/fast_sync_read.h"

const uint8_t SLAVE_ADDRESS = 0x3a;

void setup() {
  DEBUG_SERIAL.begin(115200);
  Wire.begin(SLAVE_ADDRESS);
  Wire.onRequest(requestEvent);
  Wire.onReceive(receiveEvent);

  dxl_util::init();
}

uint8_t req[64], resp[128];
volatile uint8_t req_len = 0, resp_len = 0;

byte createXORChecksum(byte *data, uint8_t len) {
  byte sum = 0x4A; // Just a random number used in both master and slave.
  for (byte i = 0; i < len; i++) {
    sum ^= data[i];
  }
  return sum;
}

void requestEvent() {
  DEBUG_SERIAL.print("reqE");
  DEBUG_SERIAL.println(resp_len, HEX);
  if (!resp_len) {
    return;
  }
  resp[0] = createXORChecksum(resp+1, resp_len);
  Wire.write(resp, resp_len+1);
  resp_len = 0;
}

void receiveEvent(int bytesReceived) {
  DEBUG_SERIAL.print("recvE");
  DEBUG_SERIAL.println(bytesReceived, HEX);
  while (Wire.available()) {
    req[req_len] = Wire.read();
    req_len++;
    if (req_len == sizeof(req))
      break;
  }
}

void handleReq() {
  if (!req_len)
    return;
  resp_len = handleCommand(req, req_len, resp+1, sizeof(resp)-1);
}

void loop() {
  delay(1);
  if (req_len) {
    handleReq();
    req_len = 0;
  }
  load_aware_goal::loop();
}
