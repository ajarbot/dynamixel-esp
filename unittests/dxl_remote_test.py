import unittest
import struct

from dxl.dxl_command import DxlCommand, DxlReadMultiCommand
from dxl import DxlRemote


class TestDxlRemote(DxlRemote):
    def __init__(self) -> None:
        self._req = []
        self._resp = {DxlCommand.INIT_POSITION_MODE: 0}

    def _expect(self, cmd, resp):
        self._resp[cmd] = resp

    def _send(self, cmd, dxl_id, req=None, resp_size=1, resp_signed=False, to_int=True):
        self._req.append((cmd, dxl_id, req, resp_size))
        if cmd == DxlCommand.GET_PRESENT_POSITION:
            return self._resp[cmd]
        elif cmd == DxlCommand.SET_GOAL_POSITION:
            return int.from_bytes(req, 'little')
        elif cmd == DxlReadMultiCommand.GET_POSITION_LOAD_TEMPERATURE:
            return self._resp[cmd]
        return None


class DummyDxlMotor:
    def __init__(self, id):
        self._dxl_id = id

    def computePresentPosition(self, pos):
        return pos

    @staticmethod
    def motors(ids):
        return [DummyDxlMotor(idd) for idd in ids]


class DxlRemoteTest(unittest.TestCase):

    def setUp(self):
        self.remote = TestDxlRemote()

    def test_get_multi_motor_position_info(self):
        self.remote._expect(
            DxlReadMultiCommand.GET_POSITION_LOAD_TEMPERATURE, bytearray(struct.pack('<ibb', 1234, 20, 25)))
        res = self.remote.get_multi_motor_position_info(
            DummyDxlMotor.motors([1]))
        self.assertEqual({1: [1234, 20, 25]}, res)

    def test_get_multi_motor_position_info_negative(self):
        self.remote._expect(DxlReadMultiCommand.GET_POSITION_LOAD_TEMPERATURE, bytearray(struct.pack(
            '<ibb', 234, -20, -25)))
        res = self.remote.get_multi_motor_position_info(
            DummyDxlMotor.motors([2]))
        self.assertEqual({2: [234, -20, -25]}, res)

    def test_get_multi_motor_position_info_multi(self):
        res = bytearray(18)
        struct.pack_into('<ibb', res, 0, 1234, 20, 25)
        struct.pack_into('<ibb', res, 6, 235, -20, -25)
        struct.pack_into('<ibb', res, 12, 3567, 100, -100)
        self.remote._expect(
            DxlReadMultiCommand.GET_POSITION_LOAD_TEMPERATURE, res)
        res = self.remote.get_multi_motor_position_info(
            DummyDxlMotor.motors([3, 4, 6]))
        self.assertEqual({3: [1234, 20, 25], 4: [
            235, -20, -25], 6: [3567, 100, -100]}, res)


if __name__ == '__main__':
    unittest.main()
