#include <_types/_uint8_t.h>
#include <cassert>
#include <cstdint>
#include <iostream>
#include <vector>

typedef uint8_t byte;

#include "../arduino/src/all_decl.h"

#define assertEquals(expected, actual)                                         \
  do {                                                                         \
    auto actual_ = actual;                                                     \
    if (expected != actual_) {                                                 \
      std::cout << "Expected " << expected << ", got " << int(actual_)         \
                << std::endl;                                                  \
    }                                                                          \
    assert(expected == actual_);                                               \
  } while (0)

enum class TestAction {
  kSetGoal,
  kGetPresent,
  kMovingStatus,
  kPresentLoadPercent,
};

std::vector<std::pair<TestAction, int32_t>> test_actions;

namespace dxl_util {

bool setGoalPosition(uint8_t id, int32_t pos) {
  assert(TestAction::kSetGoal == test_actions[0].first);
  assertEquals(test_actions[0].second, pos);
  test_actions.erase(test_actions.begin());
  return true;
}

int32_t getPresentPosition(uint8_t id) {
  auto v = test_actions[0];
  assert(TestAction::kGetPresent == v.first);
  test_actions.erase(test_actions.begin());
  return v.second;
}

uint32_t movingStatus(uint8_t id) {
  auto v = test_actions[0];
  assert(TestAction::kMovingStatus == v.first);
  test_actions.erase(test_actions.begin());
  return v.second;
}

} // namespace dxl_util

int8_t presentLoadPercent(byte id) {
  auto v = test_actions[0];
  assert(TestAction::kPresentLoadPercent == v.first);
  test_actions.erase(test_actions.begin());
  return v.second;
}

unsigned long millis() {
  static unsigned long ms = 0;
  return ms++;
}

#include "../arduino/src/load_aware_goal.h"

using load_aware_goal::State;

State getGoalStatus() {
  return static_cast<State>(load_aware_goal::getGoalStatus());
}

void test_one_step_move() {
  test_actions = {
      {TestAction::kGetPresent, 80},
      // Loop 1
      {TestAction::kPresentLoadPercent, 10},
      {TestAction::kMovingStatus, 0x0},
      {TestAction::kSetGoal, 90},
      // Loop 2
      {TestAction::kPresentLoadPercent, 10},
      {TestAction::kMovingStatus, 0x0},
      {TestAction::kSetGoal, 100},
      // Loop 3
      {TestAction::kPresentLoadPercent, 10},
      {TestAction::kMovingStatus, 0x0},
  };
  byte req[8] = {100,
                 0,
                 0,
                 0,
                 /*loop=*/200,
                 /*sleep=*/0,
                 /*load_max=*/30,
                 /*pos_inc=*/10};
  load_aware_goal::setUp(0, req, sizeof(req));
  assert(getGoalStatus() == State::kActiveMoving);
  load_aware_goal::loop();
  assert(getGoalStatus() == State::kActiveMoving);
  load_aware_goal::loop();
  assert(getGoalStatus() == State::kActiveMoving);
  load_aware_goal::loop();
  assert(test_actions.empty());
}


void test_one_step_move_negative() {
  test_actions = {
      {TestAction::kGetPresent, -80},
      // Loop 1
      {TestAction::kPresentLoadPercent, 10},
      {TestAction::kMovingStatus, 0x0},
      {TestAction::kSetGoal, -90},
      // Loop 2
      {TestAction::kPresentLoadPercent, 10},
      {TestAction::kMovingStatus, 0x0},
      {TestAction::kSetGoal, -100},
      // Loop 3
      {TestAction::kPresentLoadPercent, 10},
      {TestAction::kMovingStatus, 0x0},
  };
  byte req[8] = {0,
                 0,
                 0,
                 0,
                 /*loop=*/200,
                 /*sleep=*/0,
                 /*load_max=*/30,
                 /*pos_inc=*/10};
  *(int32_t*)(req)= -100;
  load_aware_goal::setUp(0, req, sizeof(req));
  assert(getGoalStatus() == State::kActiveMoving);
  load_aware_goal::loop();
  assert(getGoalStatus() == State::kActiveMoving);
  load_aware_goal::loop();
  assert(getGoalStatus() == State::kActiveMoving);
  load_aware_goal::loop();
  assert(test_actions.empty());
}

int main() { test_one_step_move();
test_one_step_move_negative(); }
