enum class DxlCommand {
  INIT_POSITION_MODE = 0,
  INIT_EXTENDED_POSITION_MODE = 1,
  PING = 2,
  READ_CONTROL_TABLE = 3,
  WRITE_CONTROL_TABLE = 4,
  READ_MULTI = 5,
  SET_GOAL_POSITION = 6,
  GET_PRESENT_POSITION = 7,
  LOAD_AWARE_GOAL_SET = 9,
  LOAD_AWARE_GOAL_STATUS = 10,
  FAST_SYNC_READ_MULTI = 0xE,
  DXL_EXTENDED_COMMAND = 0xF,
};

enum class DxlReadMultiCommand {
  READ_CONTROL_TABLE = 0,
  GET_POSITION_LOAD_TEMPERATURE = 1,
};

enum class DxlExtendedCommand {
  SET_ID = 0,
  GET_HOMING_OFSET = 1,
  SET_HOMING_OFFSET = 2,
  SET_PROFILE_VELOCITY_ACCELERATION = 3,
  MOVING_STATUS = 4,
  PRESENT_LOAD_PERCENT = 5,
  PRESENT_TEMPERATURE = 6,
  PRESENT_VOLTAGE = 7,
  HARDWARE_ERROR_STATUS = 8,
  GET_MODEL_INFO = 9,
};

namespace dxl_util {

struct __attribute__((packed)) ModelInfo {
  uint32_t model_info;
  uint16_t model_number;
  uint8_t firmware_version;
  uint8_t protocol_type;
};

void init();
bool setProfileVelocityAcceleration(uint8_t id, uint32_t velocity,
                                    uint32_t acceleration);
bool positionMode(uint8_t id);
bool extendedPositionMode(uint8_t id);
bool setGoalPosition(uint8_t id, int32_t pos);
int32_t getPresentPosition(uint8_t id);
bool ping(uint8_t id);
bool setID(uint8_t id, uint8_t new_id);
int32_t moving(uint8_t id);
uint32_t movingStatus(uint8_t id);
uint32_t getHomingOffset(uint8_t id);
bool setHomingOffset(uint8_t id, uint32_t offset);
int32_t presentLoad(uint8_t id);
int32_t presentTemperature(uint8_t id);
int32_t presentVoltage(uint8_t id);
int32_t hardwareErrorStatus(uint8_t id);
ModelInfo getModelInfo(uint8_t id);

int32_t read(uint8_t id, uint8_t addr, uint8_t addr_length, uint8_t *resp,
             uint8_t resp_len);
bool write(uint8_t id, uint8_t addr, const uint8_t *req, uint8_t req_len);

} // namespace dxl_util

namespace load_aware_goal {
byte setUp(byte dxl_id, const byte *req, byte req_len);
byte getGoalStatus();
void loop();
} // namespace load_aware_goal

byte fast_sync_read(uint16_t dxl_id_map, byte addr, byte len, byte *resp,
                    byte resp_len);
